#pragma once
#include <iostream>
using namespace std;

//
// rhs * result = vec
// result = rhs' * vec  : rhs'은 rhs의 역함수
//
// Matrix LU factorization (Doolittle's method)
// int r : vec 행의 갯수
// int c : vec 열의 갯수
// double ** rhs : (r x r) 메트릭스 포인터
// double ** vec : (r x c) 메트릭스 포인터 (vector)
// double ** ret : (r x c) 계산된 결과의 반환
void inverse(int r, int c, double ** rhs, double ** vec, double ** result);
