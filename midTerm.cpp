﻿// 12200603, 이가현

#include "Matrix.h"


void EX01();
void EX02();
void EX03();
void EX04();

// 1. for 문과 if 문을 사용하여 1에서 100사이의 짝수합을 구하는 프로그램을 작성하고 출력하시요.
// 2. 배열의 총합을 구하는 함수를 구성하고, 'a'라는 배열에 5의 배수 5,10,15,20,25 다섯 개를 저장해서 구현된 함수를 사용하여 총합을 출력하시요.
// 3. 최대값과 최소값을 동시에 구하는 함수를 구성하고, 'a'라는 배열에 -1,-20,10,25,10,0,12 값을 저장해서 구현된 함수를 사용하여 값을 출력하시요.
// 4. 메모리할당을 통해, 초기값을 입력받아 2차원 배열을 생성할 수 있는 함수를 구성하시요.

void main()
{
	EX01();
	EX02();
	EX03();

	int row;
	int col;
	double** a;
	// EX04
	EX04(row, col, a);
	// EX07
	double** b;
	EX07(row, col, a, b);
	// EX17
}

void EX01()
{
	int evenSum = 0;
	int n = 100;
	for (int i = 1; i <= n; i++)
	{
		if (i % 2 == 0)
		{
			evenSum = evenSum + i;
		}
	}
	cout << "EX01 = " << evenSum << endl;
}
void EX02()
{
	double a[5] = { 5,10,15,20,25 };
	double sum = 0;

	for (int i = 0; i < 5; i++)
	{
		sum = sum + a[i];
	}
	cout << "EX02 = " << sum << endl;
}
void EX03()
{
	double a[7] = { -1,-20,10,25,10,0,12 };
	double maxValue = a[0];
	double minValue = a[0];

	for (int i = 0; i < 7; i++)
	{
		if (a[i] > maxValue)
		{
			maxValue = a[i];
		}
		if (a[i] < minValue)
		{
			minValue = a[i];
		}
	}
	cout << "EX03 1) maxValue = "<< maxValue << endl;
	cout << "EX03 2) minValue = " << minValue << endl;
}


void EX04(int row, int col, double** a)
{
	cout << "Input size of row = ";
	cin >> row;
	cout << "Input size of col = ";
	cin >> col;

	a = new double* [row];
	for (int i = 0; i < row; i++)
	{ 
		a[i] = new double[col];
		for (int j = 0; j < col; j++)
		{
			cout << "Input " << j << "a[i][j] = ";
			cin >> *(a[i]+j); // �ʱⰪ �Է�
		}
		cout << endl;
	}
	cout << endl;
}

// Delete 2-d Array
void EX05(double** a)
{
	delete[] a;
}

// Print 2-d Array
void EX06(int row, int col, double** a)
{
	for (int i = 0; i < row; i++)
	{
		for (int j = 0; j < col; j++)
		{
			cout << "a[i][j] = " << *(a[i]+j);
		}
		cout << endl;
	}
	cout << endl;
}

// Copy Array a to b
void EX07(int row, int col, double** a, double** b)
{
	b = new double* [row];
	for (int i = 0; i < row; i++)
	{
		b[i] = new double[col];
		for (int j = 0; j < col; j++)
		{
			*(b[i]+j) = *(a[i]+j);
		}
	}
}

// c = a + b
void EX08(int row, int col, double** a, double** b, double** c)
{
	c = new double* [row];
	for (int i = 0; i < row; i++)
	{
		c[i] = new double[col];
		for (int j = 0; j < col; j++)
		{
			*(c[i]+j) = *(a[i]+j) + *(b[i]+j);
		}
	}
}

// c = a - b
void EX09(int row, int col, double** a, double** b, double** c)
{
	c = new double* [row];
	for (int i = 0; i < row; i++)
	{
		c[i] = new double[col];
		for (int j = 0; j < col; j++)
		{
			*(c[i]+j) = *(a[i]+j) - *(b[i]+j);
		}
	}
}

// c = a * b
void EX10(int row, int col, double** a, double** b, double** c)
{
	c = new double* [row];
	for (int i = 0; i < row; i++)
	{
		c[i] = new double[col];
		for (int j = 0; j < col; j++)
		{
			*(c[i]+j) = *(a[i]+j) - *(b[i]+j);
		}
	}
}