#include "matrixInverse.h"

//
// rhs * result = vec
// result = rhs' * vec  : rhs'은 rhs의 역함수
//
// Matrix LU factorization (Doolittle's method)
// int r : vec 행의 갯수
// int c : vec 열의 갯수
// double ** rhs : (r x r) 메트릭스 포인터
// double ** vec : (r x c) 메트릭스 포인터 (vector)
// double ** ret : (r x c) 계산된 결과의 반환
void inverse(int r, int c, double ** rhs, double ** vec, double ** ret)
{
	int s = r;

	// (0) 임시저장 메트릭스 upper / lower 생성
	double** upper = new double* [s];
	upper[0] = new double[s * s];
	for (int i = 0; i < s; i++)
	{
		upper[i] = upper[0] + s * i;
	}
	for (int i = 0; i < s; i++)
		for (int j = 0; j < s; j++)
			upper[i][j] = 0;
	//
	double** lower = new double* [s];
	lower[0] = new double[s * s];
	for (int i = 0; i < s; i++)
	{
		lower[i] = lower[0] + s * i;
	}
	for (int i = 0; i < s; i++)
		for (int j = 0; j < s; j++)
			lower[i][j] = 0;
	//
	//
	// (1) 대각행렬초기화
	for (int i = 0; i < s; i++)
	{
		lower[i][i] = 1;
	}

	// (2) upper matrix / lower matrix 초기화
	for (int i = 0; i < s; i++)
	{
		upper[0][i] = rhs[0][i];
		lower[i][0] = rhs[i][0]/upper[0][0];
	}
	for (int i = 1; i < s; i++)
	{
		for (int j = i; j < s; j++)
		{
			double tLower = 0;
			double tUpper = 0;

			if (j != i)
			{
				for (int k = 0; k < j; k++)
				{
					tLower = tLower + lower[j][k]*upper[k][i];
				}
				tLower = (rhs[j][i] - tLower) / upper[i][i];
				lower[j][i] = tLower;
			}

			for (int k = 0; k < i; k++)
			{
				tUpper = tUpper + lower[i][k] * upper[k][j];
			}

			tUpper = rhs[i][j] - tUpper;
			upper[i][j] = tUpper;
		}
	}
	//
	// (3) Forward Method
	double** temp = new double* [r];
	temp[0] = new double[r * c];
	for (int i = 0; i < s; i++)
	{
		temp[i] = temp[0] + c * i;
	}
	for (int i = 0; i < r; i++)
		for (int j = 0; j < c; j++)
			temp[i][j] = 0;
	//
	for (int i = 0; i < c; i++)
	{
		temp[0][i] = vec[0][i];
		for (int j = 1; j < r; j++)
		{
			double tLower = 0;
			for (int k = 0; k < j; k++)
			{
				tLower = tLower + lower[j][k] * temp[k][i];
			}
			tLower = vec[j][i] - tLower;
			temp[j][i] = tLower;
		}
	}
	//
	// (4) Backward Method
	for (int i = 0; i<c; i++)
	{
		ret[r - 1][i] = temp[r - 1][i] / upper[s - 1][s - 1];
		for (int j = r - 2; j >= 0; j--)
		{
			double tUpper = 0;
			for (int k = s - 1; k > j; k--)
			{
				tUpper = tUpper + upper[j][k] * ret[k][i];
			}
			tUpper = (temp[j][i] - tUpper) / upper[j][j];
			ret[j][i] = tUpper;
		}
	}
	//
	// (5) 메모리 삭제
	delete[] temp[0];
	delete[] temp;
	delete[] lower[0];
	delete[] lower;
	delete[] upper[0];
	delete[] upper;
	//
	return;
}
